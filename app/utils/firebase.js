import firebase from "firebase/app";

const firebaseConfig = {
  /**
   * FIREBASE CONFIG
   */
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
